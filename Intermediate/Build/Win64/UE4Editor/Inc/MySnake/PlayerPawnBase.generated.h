// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSNAKE_PlayerPawnBase_generated_h
#error "PlayerPawnBase.generated.h already included, missing '#pragma once' in PlayerPawnBase.h"
#endif
#define MYSNAKE_PlayerPawnBase_generated_h

#define MySnake_Source_MySnake_PlayerPawnBase_h_12_SPARSE_DATA
#define MySnake_Source_MySnake_PlayerPawnBase_h_12_RPC_WRAPPERS
#define MySnake_Source_MySnake_PlayerPawnBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MySnake_Source_MySnake_PlayerPawnBase_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend struct Z_Construct_UClass_APlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnake"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase)


#define MySnake_Source_MySnake_PlayerPawnBase_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend struct Z_Construct_UClass_APlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnake"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase)


#define MySnake_Source_MySnake_PlayerPawnBase_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public:


#define MySnake_Source_MySnake_PlayerPawnBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawnBase)


#define MySnake_Source_MySnake_PlayerPawnBase_h_12_PRIVATE_PROPERTY_OFFSET
#define MySnake_Source_MySnake_PlayerPawnBase_h_9_PROLOG
#define MySnake_Source_MySnake_PlayerPawnBase_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_PRIVATE_PROPERTY_OFFSET \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_SPARSE_DATA \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_RPC_WRAPPERS \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_INCLASS \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySnake_Source_MySnake_PlayerPawnBase_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_PRIVATE_PROPERTY_OFFSET \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_SPARSE_DATA \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_INCLASS_NO_PURE_DECLS \
	MySnake_Source_MySnake_PlayerPawnBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSNAKE_API UClass* StaticClass<class APlayerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySnake_Source_MySnake_PlayerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
